const chai = require('chai');
const assert = chai.assert;
const formatOne = require('../lib/formatter').formatOne;

describe('Formatter formatOne', () => {

	const entity = {
		id: 1,
		attr1: 'test',
		attr2: 5,
		date: '2018-10-13T13:00:29.000Z',
		deep: {
			attr1: 'deepAttr',
			attr2: 6,
			deeper: {
				attr1: 'deeperAttr',
				attr2: 7
			}
		},
		entities: [
			{
				id: 1,
				attr1: 'test1'
			},
			{
				id: 2,
				attr1: 'test2'
			}
		]
	};

	describe('Simple format', () => {
		const format = {
			attr1: 1,
			attr2: 1
		};

		const expected = {
			attr1: entity.attr1,
			attr2: entity.attr2
		};

		it('Should format entity directly', () => {
			const result = formatOne(format, entity);
			assert.deepEqual(result, expected);
		});

		it('Should save format function and then use it on entity', () => {
			const formatFunction = formatOne(format);
			const result = formatFunction(entity);
			assert.deepEqual(result, expected);
		});
	});

	it('Should get deep entity', () => {
		const format = {
			attr1: 1,
			deep: {}
		};

		const expected = {
			attr1: entity.attr1,
			deep: {
				attr1: entity.deep.attr1,
				attr2: entity.deep.attr2
			}
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});

	it('Should get only one attribute from deep entity', () => {
		const format = {
			deep: {
				attr1: 1
			}
		};

		const expected = {
			deep: {
				attr1: entity.deep.attr1
			}
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});

	it('Should get attribute from deeper entity', () => {
		const format = {
			deep: {
				deeper: {
					attr2: 1
				}
			}
		};

		const expected = {
			deep: {
				deeper: {
					attr2: entity.deep.deeper.attr2
				}
			}
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});

	it('Should get related entities in array', () => {
		const format = {
			entities: {}
		};

		const expected = {
			entities: entity.entities
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});

	it('Should get one attribute from each entity in array', () => {
		const format = {
			entities: {
				attr1: 1
			}
		};

		const expected = {
			entities: entity.entities.map(item => ({ attr1: item.attr1 }))
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});

	it('Should format date', () => {
		const format = {
			date: 'date'
		};

		const expected = {
			date: '2018-10-13'
		};

		const result = formatOne(format, entity);
		assert.deepEqual(result, expected);
	});
});