const chai = require('chai');
const assert = chai.assert;
const getRelations = require('../lib/formatter').getRelations;

describe('Formatter getRelations', () => {
	it('Should get related entities from format', () => {
		const format = {
			id: 1,
			related1: {
				id: 1
			},
			related2: {}
		};

		const expected = [
			{ name: 'related1' },
			{ name: 'related2' }];

		const relations = getRelations(format);
		assert.deepEqual(relations, expected);
	});

	it('Should get deep related entity', () => {
		const format = {
			id: 1,
			related: {
				id: 1,
				deep: {},
				deep2: {
					deeper: {}
				}
			}
		};

		const expected = [{
			name: 'related',
			relations: [{
				name: 'deep'
			}, {
				name: 'deep2',
				relations: [{
					name: 'deeper'
				}]
			}]
		}];

		const relations = getRelations(format);
		assert.deepEqual(relations, expected);
	});
});