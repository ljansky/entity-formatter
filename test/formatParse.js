const chai = require('chai');
const assert = chai.assert;
const parse = require('../lib/formatter').parse;
const parseFunction = require('../lib/parse-function');

describe('Formatter format parse', () => {

	it('Should parse simple format', () => {
		const formatString = 'id,title';
		const expected = {
			id: 1,
			title: 1
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should parse format with deep entity', () => {
		const formatString = 'id,(entity,id,title)';
		const expected = {
			id: 1,
			entity: {
				id: 1,
				title: 1
			}
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should parse format with deep entity with dots', () => {
		const formatString = 'id,entity.id';
		const expected = {
			id: 1,
			entity: {
				id: 1
			}
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should parse format with all attributes of entity', () => {
		const formatString = 'id,(entity)';
		const expected = {
			id: 1,
			entity: {}
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should parse format with all attributes of entity with .*', () => {
		const formatString = 'id,entity.*';
		const expected = {
			id: 1,
			entity: {}
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should test parse to format function', () => {
		const formatString = 'id,(test1, test2, test3)';
		const expected = {
			id: 1,
			test1: {
				test2: 1,
				test3: 1
			}
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});

	it('Should test parseFunction', () => {
		const functionString = 'deep(test1, test2(ggg(1), fff), test3())';
		const parsed = parseFunction(functionString);
		const expected = {
			name: 'deep',
			params: [
				'test1',
				'test2(ggg(1), fff)',
				'test3()'
			]
		};

		assert.deepEqual(parsed, expected);
	});

	it('Should parse format with date function', () => {
		const formatString = 'id,date(date_start)';
		const expected = {
			id: 1,
			date_start: 'date'
		};

		const format = parse(formatString);
		assert.deepEqual(format, expected);
	});
});