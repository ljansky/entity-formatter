// @flow
const R = require('ramda');
const parseFunction = require('./parse-function');
const formatFunctions = require('./format-functions');

const isObject = val => R.type(val) === 'Object';
const isArray = val => R.type(val) === 'Array';

const mergeParams = format => R.pipe(
	R.map(param => parse(param, false)),
	R.prepend(format),
	R.mergeAll
);

const parse = (formatString: string, init = true, format = {}) => {
	const parsed = parseFunction(init ? `(${formatString})` : formatString);

	if (init) {
		const r = mergeParams(format)(parsed.params);
		return r;
	} else {
		if (!parsed.params.length) {
			const path = parsed.name.split('.');
			const star = path[path.length - 1] === '*';
			const lensPath = R.lensPath(path.filter(item => item !== '*'));
			return R.set(lensPath, star ? {} : 1, {});
		} else {
			if (parsed.name !== '') {
				const [name] = parsed.params;
				return { [name]: parsed.name };
			} else {
				const [name, ...params] = parsed.params;
				const r = mergeParams(format)(params);
				return { [name]: r };
			}
		}
	}	
};

interface Relation {
	name: string,
	relations?: Array<Relation>
}

const getRelations = (format: any): Array<Relation> => {
	const relationsPipe = R.pipe(
		R.pickBy(isObject),
		R.toPairs,
		R.reduce((acc, pair) => {
			const actualItem: Relation = { name: pair[0] };
			const relations = relationsPipe(pair[1]);
			if (relations.length > 0) {
				actualItem.relations = relations;
			}

			return R.concat(acc, [actualItem]);
		}, [])
	);

	return relationsPipe(format);
};

const formatOne = (format, entity: any) => {
	const ret = R.keys(format).length === 0 ? R.pickBy(attr => !isObject(attr), entity) : {};

	for (let key in format) {
		if (format[key] === 1) {
			ret[key] = entity[key];
		} else if (typeof format[key] === 'string') {
			if (formatFunctions[format[key]]) {
				ret[key] = formatFunctions[format[key]](entity[key]);
			}
		} else if (isObject(format[key])) {
			if (isObject(entity[key])) {
				ret[key] = formatOne(format[key], entity[key]);
			}
			
			if (isArray(entity[key])) {
				ret[key] = R.map(item => formatOne(format[key], item), entity[key]);
			}
		}
	}

	return ret;
};

module.exports = {
	parse,
	getRelations,
	formatOne: R.curry(formatOne),
	parseFunction
};