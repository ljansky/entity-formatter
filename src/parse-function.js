module.exports = functionString => {
	let name = '';
	let param = '';
	const params = [];

	let opened = 0;
	let closed = 0;

	for (let c of functionString) {
		if (c === '(') {
			opened++;
			if (opened === closed + 1) {
				continue;
			}
		}

		if (c === ')') {
			closed++;
			if (opened === closed) {
				params.push(param.trim());
			}	
		}

		if (opened === 0) {
			name += c;
		} else {
			if (c === ',' && (opened === closed + 1)) {
				params.push(param.trim());
				param = '';
			} else {
				param += c;
			}
		}
	}

	return { name, params };
};